package com.only4play.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author liyuncong
 * @version 1.0
 * @file PasswordLoginModel
 * @brief 密码登录模型
 * @details 密码登录模型
 * @date 2023-11-19
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-19               liyuncong          Created
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PasswordLoginModel {
    private String redirectUri;
    private String appId;
    private String loginUsername;
    private String loginPassword;
}
