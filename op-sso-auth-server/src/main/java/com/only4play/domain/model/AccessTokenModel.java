package com.only4play.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import java.io.Serializable;

/**
 * @author liyuncong
 * @version 1.0
 * @file AccessTokenModel
 * @brief 访问token模型
 * @details 访问token模型
 * @date 2023-11-18
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-18               liyuncong          Created
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AccessTokenModel implements Serializable {
    private String grantType;
    private String appId;
    private String appSecret;
    private String code;
    private String loginUsername;
    private String loginPassword;
}
