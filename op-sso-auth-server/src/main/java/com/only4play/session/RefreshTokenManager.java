package com.only4play.session;

import com.only4play.auth.AccessTokenContent;
import com.only4play.auth.Expiration;
import com.only4play.auth.RefreshTokenContent;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * @author liyuncong
 * @version 1.0
 * @file RefreshTokenManager
 * @brief 刷新令牌管理器
 * @details 刷新令牌管理器
 * @date 2023-11-18
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-18               liyuncong          Created
 */
public interface RefreshTokenManager extends Expiration {

    /**
     * 生成refreshToken
     *
     * @param accessToken        访问令牌
     * @param accessTokenContent 访问令牌上下文
     * @return refreshToken
     */
    default String generate(String accessToken, AccessTokenContent accessTokenContent) {
        String refreshToken = "RT-" + RandomStringUtils.random(32, true, true);
        create(refreshToken, new RefreshTokenContent(accessToken, accessTokenContent));
        return refreshToken;
    }

    /**
     * 生成refreshToken
     *
     * @param refreshToken        refreshToken
     * @param refreshTokenContent 访问令牌上下文
     */
    void create(String refreshToken, RefreshTokenContent refreshTokenContent);

    /**
     * 验证refreshToken有效性，无论是否有效，都remove掉
     *
     * @param refreshToken refreshToken
     * @return 验证结果
     */
    RefreshTokenContent validate(String refreshToken);
}
