package com.only4play.session;

import com.only4play.auth.AccessTokenContent;
import com.only4play.auth.Expiration;
import com.only4play.common.utils.HttpUtils;
import com.only4play.constant.SSOConstant;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author liyuncong
 * @version 1.0
 * @file AccessTokenManager
 * @brief 访问令牌管理器
 * @details 访问令牌管理器
 * @date 2023-11-18
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-18               liyuncong          Created
 */
public interface AccessTokenManager extends Expiration {

    /**
     * 生成accessToken
     *
     * @param accessTokenContent 访问令牌上下文
     * @return 访问令牌
     */
    default String generate(AccessTokenContent accessTokenContent) {
        String accessToken = "AT-" + RandomStringUtils.random(32, true, true);
        create(accessToken, accessTokenContent);
        return accessToken;
    }

    /**
     * 生成accessToken
     *
     * @param accessToken        访问令牌
     * @param accessTokenContent 访问令牌上下文
     */
    void create(String accessToken, AccessTokenContent accessTokenContent);

    /**
     * 刷新accessToken，延长生命周期
     *
     * @param accessToken 访问令牌
     * @return 刷新结果
     */
    boolean refresh(String accessToken);

    /**
     * 查询
     *
     * @param accessToken 访问令牌
     * @return 访问令牌上下文
     */
    AccessTokenContent get(String accessToken);

    /**
     * 根据TGT删除令牌
     *
     * @param tgt tgt
     */
    void remove(String tgt);

    /**
     * 发起客户端注销请求
     *
     * @param redirect    重定向地址
     * @param accessToken 访问令牌
     */
    default void sendLogoutRequest(String redirect, String accessToken) {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(SSOConstant.LOGOUT_PARAMETER_NAME, accessToken);
        String ignore = HttpUtils.postHeader(redirect, headerMap);
    }

}
