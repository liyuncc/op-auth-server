package com.only4play.session;

import com.only4play.auth.CodeContent;
import com.only4play.auth.Expiration;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;
import java.util.UUID;

/**
 * @author liyuncong
 * @version 1.0
 * @file CodeManager
 * @brief code管理器
 * @details code管理器
 * @date 2023-11-18
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-18               liyuncong          Created
 */
public interface CodeManager extends Expiration {

    /**
     * 生成授权码
     *
     * @param tgt               tgt
     * @param sendLogoutRequest 发送注销请求
     * @param redirectUri       重定向地址
     * @return 授权码
     */
    default String generate(String tgt, boolean sendLogoutRequest, String redirectUri) {
        String code = "code-" + RandomStringUtils.random(32, true, true);
        create(code, new CodeContent(tgt, sendLogoutRequest, redirectUri));
        return code;
    }

    /**
     * 生成授权码
     *
     * @param code        code
     * @param codeContent code上下文
     */
    void create(String code, CodeContent codeContent);

    /**
     * 查找并删除
     *
     * @param code code
     * @return code上下文
     */
    CodeContent getAndRemove(String code);

    /**
     * code失效默认时间10分钟
     *
     * @return 默认失效时间
     */
    @Override
    default int getExpiresIn() {
        return 600;
    }
}
