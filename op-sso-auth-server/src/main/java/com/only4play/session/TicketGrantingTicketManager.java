package com.only4play.session;

import com.only4play.auth.AuthenticatedUser;
import com.only4play.auth.Expiration;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * @author liyuncong
 * @version 1.0
 * @file TicketGrantingTicketManager
 * @brief 登录凭证（TGT）管理器
 * @details 登录凭证（TGT）管理器
 * @date 2023-11-18
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-18               liyuncong          Created
 */
public interface TicketGrantingTicketManager extends Expiration {

    /**
     * 登录成功后，根据用户信息生成令牌
     *
     * @param authenticatedUser 用户信息
     * @return 令牌
     */
    default String generate(AuthenticatedUser<Void> authenticatedUser) {
        String tgt = "TGT-" + RandomStringUtils.random(32, true, true);
        create(tgt, authenticatedUser);
        return tgt;
    }

    /**
     * 登录成功后，根据用户信息生成令牌
     *
     * @param tgt               令牌
     * @param authenticatedUser 用户信息
     */
    void create(String tgt, AuthenticatedUser<Void> authenticatedUser);

    /**
     * 验证是否在有效期内，并更新过期时间戳
     *
     * @param tgt 令牌
     * @return 用户信息
     */
    AuthenticatedUser<Void> getAndRefresh(String tgt);

    /**
     * 设置新的用户信息
     *
     * @param tgt               令牌
     * @param authenticatedUser 用户信息
     */
    void set(String tgt, AuthenticatedUser<Void> authenticatedUser);

    /**
     * 移除
     *
     * @param tgt 令牌
     */
    void remove(String tgt);

}
