package com.only4play.service;

import com.only4play.auth.AuthenticatedUser;

/**
 * @author liyuncong
 * @version 1.0
 * @file UserService
 * @brief 用户服务
 * @details 用户服务
 * @date 2023-11-18
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-18               liyuncong          Created
 */
public interface UserService {

    AuthenticatedUser<Void> login(String loginUsername, String loginPassword);
}
