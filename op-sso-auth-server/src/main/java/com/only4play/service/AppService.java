package com.only4play.service;

/**
 * @author liyuncong
 * @version 1.0
 * @file AppService
 * @brief app服务层
 * @details app服务层
 * @date 2023-11-18
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-18               liyuncong          Created
 */
public interface AppService {

}
