package com.only4play.service.impl;

import com.only4play.auth.AuthenticatedUser;
import com.only4play.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @author liyuncong
 * @version 1.0
 * @file UserServiceImpl
 * @brief 用户服务实现
 * @details 用户服务实现
 * @date 2023-11-19
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-19               liyuncong          Created
 */
@Service
public class UserServiceImpl implements UserService {
    @Override
    public AuthenticatedUser<Void> login(String loginUsername, String loginPassword) {
        AuthenticatedUser<Void> authenticatedUser = new AuthenticatedUser<>();
        authenticatedUser.setUserId("1");
        authenticatedUser.setLoginUsername("yuncong.li@outlook.com");
        authenticatedUser.setUsername("liyuncong");
        authenticatedUser.setProperties(null);
        return authenticatedUser;
    }
}
