package com.only4play.auth;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author liyuncong
 * @version 1.0
 * @file RefreshTokenContent
 * @brief 刷新令牌上下文
 * @details 刷新令牌上下文
 * @date 2023-11-18
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-18               liyuncong          Created
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RefreshTokenContent {
    private String accessToken;
    private AccessTokenContent accessTokenContent;
}
