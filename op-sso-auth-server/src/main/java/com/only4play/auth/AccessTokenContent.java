package com.only4play.auth;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author liyuncong
 * @version 1.0
 * @file AccessTokenContent
 * @brief 访问令牌上下文
 * @details 访问令牌上下文
 * @date 2023-11-18
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-18               liyuncong          Created
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AccessTokenContent {
    private CodeContent codeContent;
    private AuthenticatedUser<Void> authenticatedUser;
    private String appId;
}
