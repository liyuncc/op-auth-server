package com.only4play.auth;

/**
 * @author liyuncong
 * @version 1.0
 * @file ExpirationPolicy
 * @brief 过期策略
 * @details 过期策略
 * @date 2023-11-18
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-18               liyuncong          Created
 */
public interface ExpirationPolicy {

    /**
     * 每5分钟执行一次
     */
    String SCHEDULED_CORN = "0 */5 * * * ?";

    /**
     * 验证过期
     */
    void verifyExpired();

}
