package com.only4play.auth;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author liyuncong
 * @version 1.0
 * @file CodeContent
 * @brief 授权码上下文
 * @details 授权码上下文
 * @date 2023-11-18
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-18               liyuncong          Created
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CodeContent implements Serializable {

    private String tgt;
    private boolean sendLogoutRequest;
    private String redirectUri;
}
