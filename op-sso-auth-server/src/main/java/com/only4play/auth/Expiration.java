package com.only4play.auth;

/**
 * @author liyuncong
 * @version 1.0
 * @file Expiration
 * @brief 到期
 * @details 到期
 * @date 2023-11-18
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-18               liyuncong          Created
 */
public interface Expiration {
    /**
     * 时效（秒）
     *
     * @return 秒
     */
    int getExpiresIn();
}
