---
title: 接口签名
date: 2024-02-05 13:43
---

# 接口签名

参考链接：
> 作者：gogogoreact
> 链接：[API接口签名(防重放攻击)](https://juejin.cn/post/6983864029550739463)
> 来源：稀土掘金
>

## 1、签名算法原理

**第一步：**

设所有发送或者接收到的数据为集合M，将集合M内非空参数值的参数按照参数名ASCII码从小到大排序（字典序），使用以下格式拼接成字符串stringA：

```text
key1value1key2value2...
```

特别注意以下重要规则：

- 参数名ASCII码从小到大排序（字典序）；
- 如果参数的值为空不参与签名；
- 参数名区分大小写；
- 传送的sign参数不参与签名；

**第二步：**

在stringA最后拼接上secret密钥或密钥标识符（可查询到密钥）得到stringSignTemp字符串；

**第三步：**

对stringSignTemp进行MD5加密得到signValue；

## 2、防重放攻击

可以通过加入 timestamp + nonce 两个参数来控制请求有效性，防止重放攻击。

### 2.1、timestamp

请求端：`timestamp`由请求方生成，代表请求被发送的时间（需双方共用一套时间计数系统）随请求参数一并发出，并将`timestamp`作为一个参数加入`sign`加密计算。
服务端：平台服务器接到请求后对比当前时间戳，设定不超过`60s`即认为该请求正常，否则认为超时并不反馈结果（由于实际传输时间差的存在所以不可能无限缩小超时时间）。
但是这样仍然是仅仅不够的，仿冒者仍然有`60秒`的时间来模仿请求进行重放攻击。所以更进一步地，可以为`sign`加上一个随机码（称之为盐值）这里我们定义为`nonce`。

### 2.2、nonce

请求端：`nonce`是由请求方生成的随机数（在规定的时间内保证有充足的随机数产生，即在`60s`内产生的随机数重复的概率为0）也作为参数之一加入`sign`签名。
服务端：服务器接受到请求先判定`nonce`是否被请求过(一般会放到redis中)，如果发现`nonce`参数在规定时间是全新的则正常返回结果，反之，则判定是重放攻击。
而由于以上2个参数也写入了签名当中，攻击方刻意增加或伪造`timestamp`和`nonce`企图逃过重放判定都会导致签名不通过而失败。

