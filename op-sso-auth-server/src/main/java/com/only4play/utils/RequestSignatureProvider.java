package com.only4play.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.only4play.common.constants.ErrorCodeEnum;
import com.only4play.common.exception.ApplicationException;
import com.only4play.common.exception.ValidationException;
import com.only4play.common.model.ValidateResult;
import com.only4play.common.utils.MD5Utils;
import com.only4play.http.RequestWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author liyuncong
 * @version 1.0
 * @file RequestSignatureProvider
 * @brief RequestSignatureProvider
 * @details RequestSignatureProvider
 * @date 2024-02-01
 * <p>
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-02-01               liyuncong          Created
 */

@Slf4j
public class RequestSignatureProvider {

    public static final String TIMESTAMP = "timestamp";
    public static final String NONCE = "nonce";
    public static final String SIGNATURE = "signature";
    public static final String APP_ID = "appId";
    public static final String ACCESS_SECRET = "secret";
    // 接口超时时间
    public static final Integer TIMEOUT = 120;

    // TODO: 模拟Redis
    private static final HashMap<String, LocalDateTime> redis = new HashMap<>();
    // TODO: 密钥配置
    private static final HashMap<String, String> appKeys = new HashMap<>();

    static {
        appKeys.put("web", "!D$5a0*W29cyGg^P");
    }

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static void main(String[] args) {
        Map<String, String[]> parameterMap = new HashMap<>();
        // 请求参数
        parameterMap.put("timestamp", new String[]{"1707202574281"});
        parameterMap.put("nonce", new String[]{"111"});
        parameterMap.put("appId", new String[]{"web"});

        // 请求体
        parameterMap.put("loginUsername", new String[]{"sunwukong"});
        parameterMap.put("loginPassword", new String[]{"@Td1$rH4u^D9v&L0"});

        String signature = signature(parameterMap, "!D$5a0*W29cyGg^P");
        log.info("signature: {}", signature);
    }

    public static String signature(RequestWrapper requestWrapper, String secret) {
        Map<String, String[]> parameterMap = requestWrapper.getParameterMap();
        // 如果不是GET请求，还需要对requestBody进行签名
        String method = requestWrapper.getMethod();
        // 处理post、delete、put请求
        if (!HttpMethod.GET.matches(method)) {
            //因为约定了终端传参的格式，所以只考虑json的情况，如果是表单传参，请自行发挥
            String contentType = requestWrapper.getContentType();
            MediaType mediaType = MediaType.parseMediaType(contentType);
            if (MediaType.APPLICATION_JSON.isCompatibleWith(mediaType)) {
                byte[] bodyBytes = requestWrapper.getBody();
                Map<String, Object> body = objectMapper.convertValue(new String(bodyBytes), new TypeReference<>() {
                });
                log.info("请求request body: \n{}", body);
                body.forEach((k, v) -> {
                    //空值或空字符串不验签
                    if (isBaseType(v) && StringUtils.isNotBlank(String.valueOf(v))) {
                        parameterMap.put(k, new String[]{String.valueOf(v)});
                    }
                });
            }
        }
        return signature(parameterMap, secret);
    }

    public static String signature(Map<String, String[]> parameterMap, String secret) {
        // 删除secret
        parameterMap.remove(ACCESS_SECRET);

        String[] keys = parameterMap.keySet().toArray(new String[0]);
        Arrays.sort(keys);
        StringBuilder temp = new StringBuilder();
        boolean first = true;
        for (String key : keys) {
            if (first) {
                first = false;
            } else {
                temp.append("&");
            }
            temp.append(key).append("=");
            String[] value = parameterMap.getOrDefault(key, null);
            String valueString = "";
            if (null != value) {
                valueString = Arrays.toString(value);
            }
            temp.append(valueString);
        }
        temp.append("&").append(ACCESS_SECRET).append("=").append(secret);
        log.info("need signature str: {}", temp);
        return MD5Utils.encrypt(temp.toString()).toUpperCase();
    }

    public static void validate(RequestWrapper requestWrapper) {
        validate(requestWrapper, false);
    }

    public static void validate(RequestWrapper requestWrapper, Boolean useTimestamp) {
        Map<String, String[]> parameterMap = new HashMap<>(requestWrapper.getParameterMap());
        // 获取签名字段
        String signature = requestWrapper.getHeader(SIGNATURE);

        // 如果不是GET请求，还需要对requestBody进行签名
        String method = requestWrapper.getMethod();
        log.info("请求方式");
        // 处理post、delete、put请求
        if (!HttpMethod.GET.matches(method)) {
            //因为约定了终端传参的格式，所以只考虑json的情况，如果是表单传参，请自行发挥
            String contentType = requestWrapper.getContentType();
            MediaType mediaType = MediaType.parseMediaType(contentType);
            if (MediaType.APPLICATION_JSON.isCompatibleWith(mediaType)) {
                byte[] bodyBytes = requestWrapper.getBody();
                log.info("请求request bodyBytes: {}", new String(bodyBytes));

                try {
                    Map<String, Object> body = objectMapper.readValue(new String(bodyBytes), new TypeReference<Map<String, Object>>() {
                    });
                    log.info("请求request body：{}", body);
                    body.forEach((k, v) -> {
                        //空值或空字符串不验签
                        if (isBaseType(v) && StringUtils.isNotBlank(String.valueOf(v))) {
                            parameterMap.put(k, new String[]{String.valueOf(v)});
                        }
                    });
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        // do validate
        validate(parameterMap, signature, useTimestamp);
    }

    public static void validate(Map<String, String[]> parameterMap, String signature, Boolean useTimestamp) {
        // 获取AppId
        String appId = parameterMap.getOrDefault(APP_ID, new String[]{null})[0];
        if (StringUtils.isBlank(appId)) {
            throw new ValidationException(List.of(ValidateResult.of(APP_ID, "AppId不能为空")));
        }

        // 获取签名参数
        if (StringUtils.isBlank(signature)) {
            throw new ValidationException(List.of(ValidateResult.of(SIGNATURE, "signature不能为空")));
        }
        String nonce = parameterMap.getOrDefault(NONCE, new String[]{null})[0];
        if (StringUtils.isBlank(nonce)) {
            throw new ValidationException(List.of(ValidateResult.of(NONCE, "nonce不能为空")));
        }

        String timestampStr = parameterMap.getOrDefault(TIMESTAMP, new String[]{null})[0];
        if (StringUtils.isBlank(timestampStr)) {
            throw new ValidationException(List.of(ValidateResult.of(TIMESTAMP, "timestamp不能为空")));
        }

        // 是否使用客服端时间戳校验
        if (useTimestamp) {
            // 校验接口请求时间戳
            // 请求传过来的时间戳与服务器当前时间戳差值大于120，则当前请求的timestamp无效
            long requestTimestamp = Long.parseLong(timestampStr);
            boolean isTimeout = Instant.now().minusSeconds(TIMEOUT).isAfter(Instant.ofEpochMilli(requestTimestamp));
            if (isTimeout) {
                throw new ValidationException(List.of(ValidateResult.of(SIGNATURE, "signature签名已过期")));
            }
        }


        // 请求传过来的随机数如果在redis中存在，则当前请求的nonce无效
        // TODO: 集成redis，暂时用map模拟
        String redisKeyPrefix = "request_repeat_";
        String signKey = redisKeyPrefix + signature;
        LocalDateTime localDateTime = redis.getOrDefault(signKey, null);
        if (Objects.nonNull(localDateTime)) {
            throw new ApplicationException(ErrorCodeEnum.TransferStatusError);
        }

        // 根据AppId获取加密密钥
        String secret = appKeys.getOrDefault(appId, null);
        if (StringUtils.isBlank(secret)) {
            throw new ValidationException(List.of(ValidateResult.of(APP_ID, "签名密钥未配置")));
        }

        // 根据请求传过来的参数构造签名，如果和接口的签名不一致，则请求参数被篡改
        HashMap<String, String[]> signMap = new HashMap<>(parameterMap);
        String currentSign = signature(signMap, secret);
        if (!signature.equalsIgnoreCase(currentSign)) {
            throw new ValidationException(List.of(ValidateResult.of(SIGNATURE, "签名不匹配")));
        }

        // 存入redis
        redis.put(signKey, LocalDateTime.now());
    }

    private static boolean isBaseType(Object o) {
        return o instanceof String
            || o instanceof Integer
            || o instanceof Double
            || o instanceof Float
            || o instanceof Long;
    }
}
