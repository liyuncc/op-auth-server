package com.only4play.web.resources;

import com.only4play.auth.AuthenticatedUser;
import com.only4play.domain.model.PasswordLoginModel;
import com.only4play.service.UserService;
import com.only4play.session.SessionManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登录资源（restfull接口）
 *
 * @author liyuncong
 * @date 2023/11/20 14:17
 **/

@Slf4j
@RestController
@RequestMapping("api/v1/login")
public class LoginResource extends AbstractBaseResource {

    @Autowired
    private UserService userService;
    @Autowired
    private SessionManager sessionManager;

    /**
     * 登录认证，并重定向到原始调用地址
     *
     * @param loginModel 登录参数
     * @param request    请求
     * @param response   响应
     * @throws IOException 异常
     */
    @PostMapping
    public void login(
        @ModelAttribute PasswordLoginModel loginModel,
        HttpServletRequest request,
        HttpServletResponse response
    ) throws IOException {
        AuthenticatedUser<Void> authenticatedUser = userService.login(loginModel.getLoginUsername(), loginModel.getLoginPassword());
        String tgt = sessionManager.setUser(authenticatedUser, request, response);
        generateCodeAndRedirect(loginModel.getRedirectUri(), tgt, response);
    }
}
