package com.only4play.web.resources;

import com.only4play.common.model.GeneralResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author liyuncong
 * @version 1.0
 * @file SignTestResource
 * @brief SignTestResource
 * @details SignTestResource
 * @date 2024-02-06
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-02-06               liyuncong          Created
 */

@Slf4j
@RestController
@RequestMapping("/signature/test")
public class SignTestResource {

    @GetMapping("/get")
    public GeneralResponse<Boolean> doGetTest(
        @RequestParam("type") String type,
        @RequestParam("name") String name
    ) {
        log.info("Request params: type={}, name={}", type, name);
        return GeneralResponse.of(Boolean.TRUE);
    }

    @PostMapping("/post")
    public GeneralResponse<Boolean> doPostTest(
        @RequestBody Map<String, String> body
    ) {
        log.info("Request body: body={}", body);
        return GeneralResponse.of(Boolean.TRUE);
    }

    @PutMapping("/put")
    public GeneralResponse<Boolean> doPutTest(
        @RequestBody Map<String, String> body
    ) {
        log.info("Request body: body={}", body);
        return GeneralResponse.of(Boolean.TRUE);
    }

    @DeleteMapping("/delete")
    public GeneralResponse<Boolean> doDeleteTest(
        @RequestBody Map<String, String> body
    ) {
        log.info("Request body: body={}", body);
        return GeneralResponse.of(Boolean.TRUE);
    }

}
