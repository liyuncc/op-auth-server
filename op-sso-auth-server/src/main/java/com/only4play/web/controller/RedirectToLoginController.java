package com.only4play.web.controller;

import com.only4play.constant.Oauth2Constant;
import com.only4play.constant.SSOConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import jakarta.servlet.http.HttpServletRequest;

/**
 * @author liyuncong
 * @version 1.0
 * @file LoginController
 * @brief 登录页面控制器
 * @details 登录页面控制器
 * @date 2023-11-19
 * <p>
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-19               liyuncong          Created
 */

@Slf4j
@Controller
@RequestMapping("/redirectToLogin")
public class RedirectToLoginController {

    /**
     * 未登录时，重定向到认证服务器登录页面
     *
     * @param redirectUri 原客户端访问地址
     * @param appId       appId
     * @param request     请求
     * @return 登录页面
     */
    @GetMapping
    public ModelAndView redirectToLogin(
        @RequestParam(value = SSOConstant.REDIRECT_URI) String redirectUri,
        @RequestParam(value = Oauth2Constant.APP_ID) String appId,
        HttpServletRequest request
    ) {
        request.setAttribute(SSOConstant.REDIRECT_URI, redirectUri);
        request.setAttribute(Oauth2Constant.APP_ID, appId);
        ModelAndView loginView = new ModelAndView();
        loginView.setViewName(SSOConstant.LOGIN_PATH);
        return loginView;
    }

}
