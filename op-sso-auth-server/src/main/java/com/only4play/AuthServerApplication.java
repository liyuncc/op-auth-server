package com.only4play;

import com.only4play.common.utils.ApplicationContextUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author liyuncong
 * @version 1.0
 * @file AuthServerApplication
 * @brief 认证服务
 * @details 认证服务
 * @date 2023-11-19
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-19               liyuncong          Created
 */
@ComponentScan(basePackages = {"com.only4play", "com.only4play.exception"})
@SpringBootApplication
public class AuthServerApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(AuthServerApplication.class, args);
        ApplicationContextUtils.setApplicationContext(context);
    }
}
