package com.only4play.constant;

/**
 * @author liyuncong
 * @version 1.0
 * @file AppConstant
 * @brief app常量
 * @details app常量
 * @date 2023-11-19
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-19               liyuncong          Created
 */
public class AppConstant {
    // 存cookie中TGT名称，和Cas保存一致
    public static final String TGC = "TGC";

    // 登录页
    public static final String LOGIN_PATH = "login";
}
